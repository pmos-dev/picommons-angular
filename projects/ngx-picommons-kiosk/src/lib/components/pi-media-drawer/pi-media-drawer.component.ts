import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsSnackService } from 'ngx-webappcommons-mobile';
import { CommonsDrawerService } from 'ngx-webappcommons-mobile';
import { ECommonsDrawerDirection } from 'ngx-webappcommons-mobile';

import { PiKioskRestMediaService } from '../../services/pi-kiosk-rest-media.service';

type TMediaRow = {
		device: string;
};

@Component({
		selector: 'pi-kiosk-media-drawer',
		templateUrl: './pi-media-drawer.component.html',
		styleUrls: ['./pi-media-drawer.component.less']
})
export class PiMediaDrawerComponent extends CommonsComponent implements OnInit {
	ECommonsDrawerDirection = ECommonsDrawerDirection;
	
	@Input() name: string = 'pi-media-drawer';
	@Input() title: string|undefined;
	@Input() disabled: boolean = false;
	
	@Output() onMediaChanged: EventEmitter<string|undefined> = new EventEmitter<string|undefined>(true);
	@Output() onBusy: EventEmitter<boolean> = new EventEmitter<boolean>(true);
	
	medias: TMediaRow[] = [];
	media: number|undefined = undefined;
	
	busy: boolean = false;
	
	constructor(
			private snackService: CommonsSnackService,
			private drawerService: CommonsDrawerService,
			private restMediaService: PiKioskRestMediaService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.drawerService.showObservable(),
				async (name: string): Promise<void> => {
					if (name !== this.name) return;
					
					await this.refreshMedias();
				}
		);
	}

	async refreshMedias(): Promise<void> {
		this.media = undefined;
		this.doMediaChanged();
		
		try {
			const medias: TMediaRow[] = [];
			
			for (const media of await (this.restMediaService.listMedias())) {
				medias.push({
					device: media
				});
			}
			
			this.medias = medias;
		} catch (e) {
			console.log(e);
		}
	}
	
	private getMediaDevice(): string|undefined {
		if (this.media === undefined || this.media >= this.medias.length) return undefined;
		return this.medias[this.media].device;
	}
	
	doMediaChanged(): void {
		this.onMediaChanged.emit(this.getMediaDevice());
	}
	
	async doEject(): Promise<void> {
		const device: string = this.getMediaDevice();
		if (!device) throw new Error('No media selected');
		
		try {
			await this.restMediaService.ejectMedia(device);
		
			this.snackService.success('Media can now be removed');

			await this.refreshMedias();
		} catch (e) {
			this.snackService.error('Unable to eject');
		} finally {
			this.busy = false;
		}
	}
}
