import { Component, Input, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';
import { CommonsDelayedService } from 'ngx-angularcommons-app';

import { CommonsSnackService } from 'ngx-webappcommons-mobile';

import { PiKioskRestSystemService } from '../../services/pi-kiosk-rest-system.service';

@Component({
		selector: 'pi-kiosk-set-time',
		templateUrl: './pi-set-time.component.html',
		styleUrls: ['./pi-set-time.component.less']
})
export class PiSetTimeComponent extends CommonsComponent implements OnInit {
	@Input() datetime: Date|undefined;
	@Input() disabled: boolean = false;

	busy: boolean = false;
	
	constructor(
			private delayedService: CommonsDelayedService,
			private snackService: CommonsSnackService,
			private restSystemService: PiKioskRestSystemService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		if (!this.datetime) this.datetime = new Date();
	}

	async doSetTime(): Promise<void> {
		if (!this.datetime) return;

		this.delayedService.delayed('setTime');
		this.busy = true;
		try {
			await this.restSystemService.setTime(this.datetime);
			this.delayedService.done('setTime');
		
			this.snackService.success('Time set successfully');
		} catch (e) {
			this.delayedService.fail('setTime');
			this.snackService.error('Unable to set time');
		} finally {
			this.busy = false;
		}
	}
}
