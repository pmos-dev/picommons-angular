import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsMobileFormModule } from 'ngx-webappcommons-mobile-form';
import { NgxWebAppCommonsTableModule } from 'ngx-webappcommons-table';

import { PiSetTimeComponent } from './components/pi-set-time/pi-set-time.component';
import { PiMediaDrawerComponent } from './components/pi-media-drawer/pi-media-drawer.component';

import { PiKioskRestSystemService } from './services/pi-kiosk-rest-system.service';
import { PiKioskRestMediaService } from './services/pi-kiosk-rest-media.service';

@NgModule({
		imports: [
				CommonModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsAppModule,
				NgxWebAppCommonsMobileModule,
				NgxWebAppCommonsMobileFormModule,
				NgxWebAppCommonsTableModule,
				NgxAngularCommonsPipeModule
		],
		declarations: [
				PiSetTimeComponent,
				PiMediaDrawerComponent
		],
		exports: [
				PiSetTimeComponent,
				PiMediaDrawerComponent
		]
})
export class NgxPiCommonsKioskModule {
	static forRoot(): ModuleWithProviders {
		return {
				ngModule: NgxPiCommonsKioskModule,
				providers: [
						PiKioskRestSystemService,
						PiKioskRestMediaService
				]
		};
	}
}
