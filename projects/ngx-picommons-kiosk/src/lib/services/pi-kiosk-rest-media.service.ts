import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { NodeAppCommonsRestService } from 'ngx-nodeappcommons-angular';

@Injectable({
	providedIn: 'root'
})
export class PiKioskRestMediaService extends NodeAppCommonsRestService {
	constructor(
			http: HttpClient,
			configService: CommonsConfigService
	) {
		super(http, configService);
	}

	public async listMedias(): Promise<string[]> {
		return await this.get<string[]>('/medias');
	}

	public async listFiles(mount: string, extension?: string): Promise<string[]> {
		const params: { extension?: string } = {};
		if (extension) params.extension = extension;
		
		return await this.get<string[]>(`/medias/${mount}`, params);
	}

	public async ejectMedia(mount: string): Promise<void> {
		await this.delete(`/medias/${mount}`);
	}
}
