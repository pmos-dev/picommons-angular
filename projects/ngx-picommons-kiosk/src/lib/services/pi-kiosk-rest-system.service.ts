import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsDate } from 'tscommons-core';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { NodeAppCommonsRestService } from 'ngx-nodeappcommons-angular';
import { ECommonsContentType } from 'ngx-httpcommons-rest';

@Injectable({
	providedIn: 'root'
})
export class PiKioskRestSystemService extends NodeAppCommonsRestService {
	constructor(
			http: HttpClient,
			configService: CommonsConfigService
	) {
		super(http, configService);
	}

	public async setTime(time: Date): Promise<void> {
		await this.patch(
				'/system/time',
				{
						timestamp: CommonsDate.dateToYmdHis(time)
				},
				undefined, undefined,
				ECommonsContentType.JSON
		);
	}

	public async shutdown(): Promise<void> {
		await this.delete('/system');
	}
}
