import { Component, Input, Output, OnInit, AfterViewInit, EventEmitter } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { PiSocketIoHostService } from 'ngx-picommons-kiosk';

import { IPiUpdateVersion } from '../../interfaces/ipi-update-version';

type TMediaRow = {
		device: string;
};

@Component({
		selector: 'pi-local-source',
		templateUrl: './pi-local-source.component.html',
		styleUrls: ['./pi-local-source.component.less']
})
export class PiLocalSourceComponent extends CommonsComponent implements OnInit, AfterViewInit {
	@Input() current: number = 0;
	
	@Input() available: IPiUpdateVersion|undefined;
	@Output() availableChange: EventEmitter<IPiUpdateVersion|undefined> = new EventEmitter<IPiUpdateVersion|undefined>();

	@Output() onUpdate: EventEmitter<string> = new EventEmitter<string>(true);

	medias: TMediaRow[] = [];
	media: number|undefined = undefined;

	constructor(
			private hostService: PiSocketIoHostService
	) {
		super();
	}

	async ngOnInit(): Promise<void> {
		super.ngOnInit();

		this.current = (await this.hostService.send('update/current', '')) as number;

		await this.refreshMedias();
	}

	async ngAfterViewInit(): Promise<void> {
		super.ngAfterViewInit();
		
		await this.refreshMedias();
	}

	async refreshMedias(): Promise<void> {
		this.media = undefined;
		this.availableChange.emit(undefined);
		
		try {
			const medias: TMediaRow[] = [];
			
			for (const media of await (await this.hostService.send('update/source', ''))) {
				medias.push({
					device: media
				});
			}
			
			this.medias = medias;
		} catch (e) {
			console.log(e);
		}
	}
	
	async doCheck(): Promise<void> {
		if (this.media === undefined) throw new Error('No media selected');
		if (this.media >= this.medias.length) throw new Error('Media is out of range');
		
		this.availableChange.emit(undefined);
		
		try {
			const encoded: TEncodedObject|undefined = await this.hostService.send('update/media', this.medias[this.media].device);
			if (encoded !== undefined) {
				const latest: IPiUpdateVersion = CommonsType.decodePropertyObject(encoded) as IPiUpdateVersion;
				this.availableChange.emit(latest.version > this.current ? latest : undefined);
			}
		} catch (e) {
			console.log(e);
		}
	}

	doUpdate(): void {
		if (this.media === undefined) throw new Error('No media selected');
		if (this.media >= this.medias.length) throw new Error('Media is out of range');

		this.onUpdate.emit(this.medias[this.media].device);
	}
}
