import { Component, OnInit } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';
import { CommonsDelayedService } from 'ngx-angularcommons-app';

import { CommonsSnackService } from 'ngx-webappcommons-mobile';
import { CommonsDialogService } from 'ngx-webappcommons-app';

import { PiSocketIoHostService } from 'ngx-picommons-kiosk';

import { IPiUpdateVersion } from '../../interfaces/ipi-update-version';

import { EPiUpdateType } from '../../enums/epi-update-type';

@Component({
		selector: 'pi-update-check',
		templateUrl: './pi-update-check.component.html',
		styleUrls: ['./pi-update-check.component.less']
})
export class PiUpdateCheckComponent extends CommonsComponent implements OnInit {
	EPiUpdateType = EPiUpdateType;
	
	type: EPiUpdateType|undefined;
	
	checking: boolean = false;
	available: IPiUpdateVersion|undefined;
	updating: boolean = false;
	current: number = 0;

	constructor(
			private hostService: PiSocketIoHostService,
			private snackService: CommonsSnackService,
			private dialogService: CommonsDialogService,
			private delayedService: CommonsDelayedService
	) {
		super();
	}

	async ngOnInit(): Promise<void> {
		super.ngOnInit();

		this.type = (await this.hostService.send('update/type', '')) as EPiUpdateType;
		this.current = (await this.hostService.send('update/current', '')) as number;
		
		await this.doCheck();
	}

	private async doCheck(): Promise<void> {
		this.checking = true;
		
		const encoded: TEncodedObject|undefined = await this.hostService.send(
				`update/${this.type === EPiUpdateType.REMOTE ? 'remote' : 'media'}`,
				''
		);
		if (encoded === undefined) this.available = undefined;
		else {
			const latest: IPiUpdateVersion = CommonsType.decodePropertyObject(encoded) as IPiUpdateVersion;
			this.available = latest.version > this.current ? latest : undefined;
		}
		
		console.log(this.available);
		this.checking = false;
	}

	doUpdate(path?: string): void {
		console.log(path);
		this.dialogService.confirm(
				'Software update',
				'Apply this update to the system?',
				async (): Promise<void> => {
					this.updating = true;
					
					this.delayedService.delayed('update-install');

					console.log(this.available);
					let data: {} = this.available!;
					if (this.type === EPiUpdateType.MEDIA) data = Object.assign({}, data, { media: path });
					
					if (await this.hostService.send(
							'update/install',
							CommonsType.encodePropertyObject(data)
					)) {
						this.updating = false;
						this.delayedService.done('update-install');
						
						this.dialogService.ok(
								'Software update',
								'Update successfully applied. Will now shutdown.',
								(): void => {
									this.hostService.shutdown();
								}
						);
					} else {
						this.delayedService.fail('update-install');
						this.updating = false;
						this.snackService.error('Failure applying update');
					}
				},
				(): void => {
					// do nothing
				}
		);
	}
}
