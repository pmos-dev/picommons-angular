import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsTableModule } from 'ngx-webappcommons-table';

import { PiUpdateCheckComponent } from './components/pi-update-check/pi-update-check.component';
import { PiLocalSourceComponent } from './components/pi-local-source/pi-local-source.component';

@NgModule({
		imports: [
				CommonModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsAppModule,
				NgxWebAppCommonsMobileModule,
				NgxAngularCommonsPipeModule,
				NgxWebAppCommonsTableModule
		],
		declarations: [
				PiUpdateCheckComponent,
				PiLocalSourceComponent
		],
		exports: [
				PiUpdateCheckComponent
		]
})
export class NgxPiCommonsUpdateModule { }
