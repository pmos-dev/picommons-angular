import { CommonsType } from 'tscommons-core';

export interface IPiUpdateVersion {
	software: string;
	version: number;
	timestamp: Date;
}

export function isIPiUpdateVersion(test: any): test is IPiUpdateVersion {
	if (!CommonsType.isObject(test)) return false;
	
	const attempt: IPiUpdateVersion = test as IPiUpdateVersion;
	
	if (!CommonsType.hasPropertyString(attempt, 'software')) return false;
	if (!CommonsType.hasPropertyNumber(attempt, 'version')) return false;
	if (!CommonsType.hasPropertyDate(attempt, 'timestamp')) return false;

	return true;
}
